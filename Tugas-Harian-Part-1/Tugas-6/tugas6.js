//soal 1
console.log("---Soal1---")
const luasLingkaran = (r) => {
	const pi = r % 7 === 0 ? (22/7) : 3.14
	let luas = pi * r * r
	return luas
}

const kelilingLingkaran = (r) => {
	const pi = r % 7 === 0 ? (22/7) : 3.14
	let keliling = 2 * pi * r 
	return keliling 
}

console.log(luasLingkaran(7))
console.log(kelilingLingkaran(7))

console.log()

console.log("---Soal2---")
let kalimat = ""

const tambahKalimat = (kata)=>{
  kalimat = `${kalimat} ${kata}`
}

tambahKalimat(' saya ')
tambahKalimat(' adalah ')
tambahKalimat(' seorang ')
tambahKalimat(' frontend ')
tambahKalimat(' develover')

console.log(kalimat)

console.log()

//soal3
console.log("---Soal3---")
const newFunction = literal = (firstName, lastName)=>{ 
    return {
    firstName,
    lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`)
    }
    }
  }


//Driver Code 
newFunction("William", "Imoh").fullName()

//soal4
console.log("---Soal4---")
let newObject = {
  firstName: 'Harry',
  lastName: 'Potter Holt',
  destination: 'Hogwarts React Conf',
  occupation: 'Deve-wizart Avocado',
  spell: 'Vimulus Renderus!!!'
}

const {firstName, lastName, destination, occupation, spell} = newObject


// Driver code
console.log(firstName, lastName, destination, occupation, spell)
console.log()

//soal5
console.log("---Soal5---")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined =[...west, ...east]

//Driver Code
console.log(combined)
