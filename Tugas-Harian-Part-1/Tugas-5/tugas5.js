//soal1

console.log ('---Soal1---')

function halo() {
  return 'Halo Sanbers!'
}

console.log(halo())

//soal2
console.log('---Soal2---')

function kalikan(num1,num2) {
  return num1 * num2
}

var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) 
console.log()

//soal3 
console.log('---Soal3---')
function introduce(name,age,address,hobby){
	var kalimat = "Nama saya" + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!"
  // return 'nama saya' + name + ', umur saya ' + age + ' tahun, alamat saya di ' + address + ', dan saya punya hobby yaitu'+ hobby;
  	return kalimat 
}
var name = " John"
var age =  30
var address = "Jalan belum jadi"
var hobby = " Gaming"
 
var perkenalan = introduce(name,age,address,hobby)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan belum jadi, dan saya punya hobby yaitu Gaming!" 
console.log()

//soal4
console.log('---soal4---')
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
// var arrayDaftarPeserta = {
//   nama : "asep",
//   "jeniskelamin" : "lakilaki",
//   hobbie : "baca buku",
//   "tahunlahir": 1992
var objectPeserta = {
	nama : arrayDaftarPeserta[0],
	jenisKelamin : arrayDaftarPeserta[1],
	hobi : arrayDaftarPeserta[2],
	tahunLahir : arrayDaftarPeserta[3],
}


console.log(objectPeserta)
console.log()

//soal5
console.log ('---soal5---')
// function buatObjectFruit(nama,warna,adabijinya,harga){
//   var fruit = {};
//   fruit.nama =nama;
//   fruit.warna = warna;
//   fruit.adabijinya = adabijinya;
//   fruit.harga = harga;
//   return fruit;
// }

// var fruit1 =buatObjectFruit('strawberry','merah', 'false', '9000')
// var fruit2 =buatObjectFruit('jeruk','orange', 'true', '8000')
// var fruit3 =buatObjectFruit('Semangka','Hijau & Merah', 'true', '10000')
// var fruit4 =buatObjectFruit('Pisang','Kuning', 'false', '5000')


// console.log(fruit1)

var buahBuah = [
{
	nama: "strawberry",
	warna: "merah",
	adaBijinya: false,
	harga: 9000
},
{
	nama: "jeruk",
	warna: "orange",
	adaBijinya: true,
	harga: 8000
},
{
	nama: "Semangka",
	warna: "Hijau & Merah",
	adaBijinya: true,
	harga: 10000
},
{
	nama: "Pisang",
	warna: "kuning",
	adaBijinya: false,
	harga: 5000 
}
]

console.log(buahBuah[0])
console.log()

//soal6
console.log('---Soal6---')
// function dataFilm(nama, durasi, genre, tahun){
//   this.nama = nama;
//   this.durasi = durasi;
//   this.genre = genre;
//   this.tahun = tahun;
// }

// var dataFilms = [
//         {
//             nama: "thor",
//             durasi: "2 jam",
//             genre: "action",
//             tahun: 2015
//         },
//         {
//             nama: "avenger",
//             durasi: "2 jam",
//             genre: "action",
//             tahun: 2017
//         },
 
//           {
//             nama: "junon",
//             durasi: "1 jam",
//             genre: "horror",
//             tahun: 2018
//         }
//     ];


// dataFilms.push(new dataFilm("upin", "30 menit","slince of life", "2013"))

// console.log(dataFilms)

var dataFilm = []

function tambahDataFilm(nama, durasi, genre, tahun){
	dataFilm.push(
	{
		nama: nama,
		durasi: durasi,
		genre: genre,
		tahun: tahun
	})
}

tambahDataFilm("lotr", "2 jam", "action", "1999")
tambahDataFilm("avenger", "2 jam", "action", "2019")
tambahDataFilm("spiderman", "2 jam", "action", "2004")
tambahDataFilm("juon", "2 jam", "horror", "2004")

console.log(dataFilm)
console.log