// soal 1
var string1 = 'saya';
var string2 = 'senang';
var string3 = 'belajar';
var string4 = 'javascript';
var soal1 = string1 + " " + (string2[0].toUpperCase() + string2.slice(1)) 
+ " " + string3 + " " + string4.toUpperCase();
// console.log(string1.concat(" "+string2, " "+string3, " "+string4.toUpperCase() )); // saya senang belajar JAVASCRIPT
console.log("----Soal1-----")
console.log(soal1)
console.log()

// soal 2

var str1 = "1";
var str2 = "2";
var str3 = "4";
var str4 = "5";

var soal2 = parseInt(str1) + parseInt(str2) + parseInt(str3) + parseInt(str4)
// var num1 = parseInt(str1);
// var num2 = parseInt(str2);
// var num3 = parseInt(str3);
// var num4 = parseInt(str4);
console.log("---Soal2---")
console.log(soal2)
console.log()
// console.log(num1+num2+num3+num4);

// soal 3

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); //wah
var kataKedua = kalimat.substring(4, 14); // javascript
var kataKetiga = kalimat.substring(15, 18); // itu
var kataKeempat = kalimat.substring(19, 24); // keren
var kataKelima = kalimat.substring(25, 31);  // sekali 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
console.log();

// soal 4

var nilai = 45;

if(nilai >= 80){
  console.log("indeksnya A")
} else if(nilai >= 70 && nilai < 80) {
  console.log("indeksnya B")
} else if (nilai >= 60 && nilai < 70) {
  console.log ("indeksnya c")
} else if (nilai >= 50 && nilai < 60) {
  console.log("indeksnya D")
} else {
  console.log("indeksnya E")
}

console.log();

// soal 5
console.log("----Soal 5----");
var tanggal = 8;
var bulan = 8;
var tahun = 2000;
var strBulan 
switch(bulan) {
  case 1:   {strBulan = "Januari"; break; }
  case 2:   {strBulan = "Februari"; break;}
  case 3:   {strBulan = "Maret"; break;}
  case 4:   {strBulan = "April"; break;}
  case 5:   {strBulan = "Mei"; break;}
  case 6:   {strBulan = "Juni"; break;}
  case 7:   {strBulan = "Juli"; break;}
  case 8:   {strBulan = "Agustus"; break;}
  case 9:   {strBulan = "September"; break;}
  case 10:   {strBulan = "Oktober"; break;}
  case 11:   {strBulan = "November"; break;}
  case 12:   {strBulan = "Desember"; break;}
  default:   {strBulan = "Invalid"; }
}

console.log(tanggal + " " +strBulan + " " + tahun)
console.log()
  
