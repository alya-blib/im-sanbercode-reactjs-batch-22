// soal 1

// var str = 'LOOPING PERTAMA'; {
// console.log(str);
// }
// var num = 0;
// while( num <= 18 ) {
//   num += 2 ;
//   console.log(num + '- I love coding');

// } 

// var str = 'LOOPING KEDUA'; {
// console.log(str);
// }
// var num1 = 22;
// while( num1> 2 ) {
//   num1 -= 2 ;
//   console.log(num1 + '- I will become a frontend developer');

// } 

console.log("---Soal 1---")

var iterasiLoopPertama = 1;
var iterasiLoopKedua = 20;

console.log('LOOPING PERTAMA')
while ( iterasiLoopPertama <=20){
  if (iterasiLoopPertama % 2 === 0){
    console.log(iterasiLoopPertama + " - I love coding")
  }
  iterasiLoopPertama++
}

console.log('LOOPING KEDUA')
while ( iterasiLoopKedua > 0){
  if (iterasiLoopKedua % 2 === 0){
    console.log(iterasiLoopKedua + " - I will become a frontend developer")
  }
  iterasiLoopKedua--
}

console.log()


//soal 2

// var jumlah = 20;

// for(var num = 1; num <= jumlah; num++){

// if(num % 2 === 0){
//   console.log(num + " - " + "Berkualitas");
// }
// else if(num % 3 === 0 && num % 2 !== 0){
//   console.log(num + " - " + "I Love Coding");
// }
// else {
//   console.log(num + " - " + "Santai");
// }

// }

console.log("---Soal 2---")
for(var i = 1; i <= 20; i++) {
  if (i % 3 === 0 && i % 2 === 1){
    console.debug(i + " - I Love Coding")
  }else if(i % 2 === 1){
    console.debug(i + " -Santai")
    }else{
      console.debug(i + " - Berkualitas")
    }
  }
console.log()

// soal 3

//  {
//     var totalNumberofRows = '6';
//     var output = '#';
//     for (var i = 0; i <= 6; i++) {
//         for (var j = 0; j < i; j++) {
//             output += '#' ;
//         }
//         console.log(output);
//         output = '#';
//     }
// }

console.log("---Soal 3---")
for(var i = 1; i <= 7; i++) {
  var x = "";
  for(var j = 1; j <= i; j++) {
    x = x + '#';
  }
  console.log(x);
}
console.log()


// soal 4
// var kalimat="saya sangat senang belajar javascript"
// var words = kalimat.split(" ")
// console.log(words)
console.log("---Soal 4---")
var kalimat="saya sangat senang belajar javascript"
var soal4 = kalimat.split(" ")
console.log(soal4)
console.log()

// soal 5
// var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
// daftarBuah.sort()
// for (var i=0; i < daftarBuah.length; i++)
//    consol'e.log(daftarBuah[i]);
console.log("---Soal5---")
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var sortDaftarBuah = daftarBuah.sort()
for (var i=0; i < daftarBuah.length; i++){
   console.log(sortDaftarBuah[i]);
}

console.log()
